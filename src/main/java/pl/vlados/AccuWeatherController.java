package pl.vlados;

import org.codehaus.jettison.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.vlados.api.AccuWeatherForecastAPI;
import pl.vlados.dto.response.Day;
import pl.vlados.dto.response.ForecastFiveDaysResponse;
import pl.vlados.dto.weather5daysAPI.DailyForecast;
import pl.vlados.dto.weather5daysAPI.Forecast5DaysResponse;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/forecast/v1")
public class AccuWeatherController {

    @Autowired
    Environment environment;

    private static String API_PARAM = "apikey";

    @Value("${api_key}")
    private String API_KEY;

    @Autowired
    private AccuWeatherForecastAPI forecastAPI;

    private Map<String, ForecastFiveDaysResponse> forecast5daysCache = new HashMap<>();

    @GetMapping("/5days/{location_id}")
    public ForecastFiveDaysResponse getWeatherFor5DaysByLocation(@PathVariable(value = "location_id") String locationId) throws IOException, JSONException {

        Forecast5DaysResponse jsonStr;
        ForecastFiveDaysResponse response = null;
        boolean callAPI = false;

        if (!locationId.isEmpty()) {

            if (forecast5daysCache.containsKey(locationId) && forecast5daysCache.get(locationId).getDayList().get(0).getDate().contains(LocalDate.now().toString())) {
                System.out.println("Get from cache");
                response = forecast5daysCache.get(locationId);
            } else {
                jsonStr = forecastAPI.getWeatherFor5DaysByLocation(API_KEY, false, locationId);

                List<DailyForecast> daysFromAPI = jsonStr.getDailyForecasts();

                List<Day> dayList = new ArrayList<>(daysFromAPI.size());
                for (DailyForecast dApi : daysFromAPI) {
                    dayList.add(new Day(dApi.getDate(),dApi.getTemperature().getMinimum().getValue(),dApi.getTemperature().getMaximum().getValue()));
                }

                response = new ForecastFiveDaysResponse(environment.getProperty(""), callAPI, locationId,  dayList.get(0).getDate(), dayList);
                callAPI = true;
                forecast5daysCache.put(locationId, response);
            }

        }

        if (response != null) {
            response.setUseAccuweatherAPI(callAPI);
        }

        return response;
    }
}
