package pl.vlados.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import pl.vlados.dto.weather5daysAPI.Forecast5DaysResponse;

@FeignClient(value = "accuweather-service", url = "http://dataservice.accuweather.com")
public interface AccuWeatherForecastAPI {

    @GetMapping("/forecasts/v1/daily/5day/{location_id}")
    Forecast5DaysResponse getWeatherFor5DaysByLocation(@RequestParam(value = "apikey") String apikey,
                                                       @RequestParam(value = "metric") boolean metric,
                                                       @PathVariable(value = "location_id") String locationId);

}
