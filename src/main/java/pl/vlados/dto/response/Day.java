package pl.vlados.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Day {

    private String date;
    private Double minTemp;
    private Double maxTemp;
}