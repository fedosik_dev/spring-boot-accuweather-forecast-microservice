package pl.vlados.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ForecastFiveDaysResponse extends Response {

    private String locationKey;
    private String requestDate;

    private List<Day> dayList;

    public ForecastFiveDaysResponse(String port, boolean callAPI, String locationKey, String requestDate, List<Day> dayList) {
        super(port, callAPI);
        this.locationKey = locationKey;
        this.requestDate = requestDate;
        this.dayList = dayList;
    }
}
