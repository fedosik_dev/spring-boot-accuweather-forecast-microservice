package pl.vlados.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
abstract class Response {

    protected String port;
    protected boolean useAccuweatherAPI;

    public Response(String port, boolean useAccuweatherAPI) {
        this.port = port;
        this.useAccuweatherAPI = useAccuweatherAPI;
    }
}
